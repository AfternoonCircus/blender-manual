.. _bpy.ops.mesh.extrude_vertices_move:

****************
Extrude Vertices
****************

.. reference::

   :Mode:      Edit Mode
   :Menu:      :menuselection:`Vertex --> Extrude Vertices`,
               :menuselection:`Mesh --> Extrude --> Extrude Vertices`
   :Shortcut:  :kbd:`E`

Extrude vertices as individual vertices.

.. list-table::

   * - .. figure:: /images/modeling_meshes_editing_vertex_extrude-vertices_before.png
          :width: 320px

          Vertex selected.

     - .. figure:: /images/modeling_meshes_editing_vertex_extrude-vertices_after.png
          :width: 320px

          Vertices extrude.
