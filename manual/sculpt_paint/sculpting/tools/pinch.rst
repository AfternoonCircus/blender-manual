
*****
Pinch
*****

.. reference::

   :Mode:      Sculpt Mode
   :Tool:      :menuselection:`Toolbar --> Pinch`
   :Shortcut:  :kbd:`P`

Pulls geometry towards the center of the brush.
When inverted via :kbd:`Ctrl` it will *Magnify* geometry
by pushing them away from the center of the brush.


Brush Settings
==============

General
-------

.. note::

   More info at :ref:`sculpt-tool-settings-brush-settings-general` brush settings
   and on :ref:`sculpt-tool-settings-brush-settings-advanced` brush settings.
